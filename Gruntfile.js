var path = require('path');

module.exports = function(grunt) {

    var ENV = require('./ENV.json');

    grunt.initConfig({
        clean: [
            'dist/'
        ],
        copy: {
            main: {
                expand: true,
                cwd: 'src/',
                src: '**',
                dest: 'dist/',
                flatten: false, // false in order to preserve directory structure so we can flatten through rename().
                filter: 'isFile',
                options: {
                    process: function (content, srcpath) {
                        const requireStart = 'require(\'';
                        const requireEnd = '\');';
                        var startPos = content.indexOf(requireStart);

                        while (startPos !== -1) {
                            var endPos = content.indexOf(requireEnd, startPos);
                            var file = content.substring(startPos + requireStart.length, endPos);

                            var filePath = path.resolve(srcpath, '../', file).split(/[/\\]/);
                            var srcIndex = filePath.indexOf('src'); // Set to same as grint.initConfig.copy.main.src.
                            var newFile = './' + filePath.slice(srcIndex + 1).join('.');

                            content = content.replace(requireStart + file + requireEnd,
                                requireStart + newFile + requireEnd);

                            startPos = content.indexOf(requireStart,
                                endPos + requireEnd.length + (newFile.length - file.length));
                        }

                        return content;
                    },
                },
                rename: function(dest, src) {
                    var newSrc = src.split(/[/\\]/).join('.');
                    return dest + newSrc;
                }

            },
        },
        screeps: {
            options: {
                email: ENV.world.email,
                password: ENV.world.password,
                branch: 'default',
                ptr: false
            },
            dist: {
                src: ['dist/*.js', 'dist/*.json']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-screeps');

    grunt.registerTask('build', ['clean', 'copy']);
    grunt.registerTask('test',  function() {
        grunt.task.run('build');
        grunt.config('screeps.options.branch', 'test');
        grunt.task.run('screeps');
    });
    grunt.registerTask('deploy', ['build', 'screeps']);
}