/**
 * The Scheduler is a min priority heap that only allows the pop() operation when the priority of the min element is
 * less than Game.time. The Scheduler allows objects to be scheduled for pop() only after a certain amount of ticks have
 * passed by adding the desired amount of ticks to Game.time.
 *
 * > var scheduler = new Scheduler();
 * > scheduler.push('World', 3);
 * > scheduler.push('Hello', 1);
 * > Game.time
 * 0
 * > scheduler.pop();
 * null
 * > Game.time
 * 1
 * > scheduler.pop();
 * 'Hello'
 * > scheduler.pop();
 * null
 * > Game.time
 * 2
 * > Game.time
 * 3
 * > scheduler.pop();
 * 'World'
 */
class Scheduler {

    /**
     * Creates the Scheduler from scratch or from an existing min heap 
     * represented as an array. The behaviour of the Scheduler is undefined 
     * if the given array is not a min heap.
     * 
     * @param  {Array=} heap an existing min heap to use for the Scheduler.
     */
    constructor(heap) {
        if (_.isArray(heap)) {
            this.heap = heap;
        } else {
            this.heap = [null];
        }
    }

    /**
     * Private method. Percolates up the element at index i.
     * 
     * @param  {Number} i the index of the element to percolateUp.
     */
    percolateUp(i) {
        while (_.floor(i / 2) > 0) {
            
            if (this.heap[i].v < this.heap[_.floor(i / 2)].v) {
                var tmp = this.heap[_.floor(i / 2)];
                this.heap[_.floor(i / 2)] = this.heap[i];
                this.heap[i] = tmp;
            }

            i = _.floor(i / 2);
        }          
    }

    /**
     * Push a new object x into the Scheduler with a priority v.
     * 
     * @param  {Object} x the object to push into the scheduler.
     * @param  {Number} v the priority of the object.
     */
    push(x, v) {
        this.heap.push({ x: x, v: v });
        this.percolateUp(this.heap.length - 1);
    }

    /**
     * Private method. Returns the index of the max child of the element at
     * index i.
     * 
     * @param  {Number} i the element to find the max child of.
     * @return {Number}   the index of the max child of the element at i.
     */
    minChild(i) {
        if ((i * 2 + 1) > this.heap.length - 1) {
            return i * 2;
        } else {
            if (this.heap[i * 2].v < this.heap[i * 2 + 1].v) {
                return i * 2;
            } else {
                return i * 2 + 1;
            }
        }
    }

    /**
     * Private method. Percolates down the element at index i.
     * 
     * @param  {Number} i the index of the element to percolateUp.
     */
    percolateDown(i) {
        while ((i * 2) <= this.heap.length - 1) {
            var j = this.minChild(i);

            if (this.heap[i].v > this.heap[j].v) {
                var tmp = this.heap[i];
                this.heap[i] = this.heap[j];
                this.heap[j] = tmp;
            }

            i = j;
        }
    }

    /**
     * Pops an object with a priority of 0, and return it. If no such object 
     * exists, returns null.
     * 
     * @return {Object} an object with a priority of 0.
     */
    pop() {
        if (this.heap.length < 2) {
            return null;
        }

        var min = this.heap[1];

        if (min.v > Game.time) {
            return null;
        }
        
        this.heap[1] = this.heap[this.heap.length - 1];
        this.heap.pop();
        this.percolateDown(1);
        return min.x;
    }

    /**
     * Returns the object with the lowest priority in the scheduler without
     * removing it from the scheduler. If no such object exists, returns null.
     * 
     * @return {Object} the object with the lowest priority in the scheduler.
     */
    peak() {
        if (this.heap.length < 2) {
            return null;
        }

        return this.heap[1].x;
    }

    /**
     * Use only the encapsulated array for JSON.stringify. Easier to persist 
     * this in memory this way.
     * 
     * @return {Object} the encapsulated.
     */
    toJSON () {
        return this.heap;
    }

    /**
     * The number of elements in the scheduler.
     * @property {Number} length the number of elements in the scheduler.
     */
    get length() {
        return this.heap.length - 1;
    }

}

module.exports = Scheduler;