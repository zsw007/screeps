/**
 * Events that can be listened to synchronously from within a class. To use, create a new SynchronousEvent, and listen()
 * to a event name by passing it the context (i.e. an instance of the object "this", or null if static), and the
 * function to call when the event fires. Multiple events are supported within a single SynchronousEvent.
 */
class SynchronousEvent {

    constructor() {
        // A mapping of { event: function }.
        this.listeners = {};
    }

    /**
     * Listen to the event.
     *
     * @param {String}   event    the name of the event to listen to.
     * @param {Object}   context  the context of the callback function. "this" in most cases, "null" if static.
     * @param {Function} callback the callback function when the event is fired.
     */
    listen(event, context, callback) {
        if (event in this.listeners) {
            this.listeners[event].push({
                context: context,
                callback: callback
            });
        } else {
            this.listeners[event] = [{
                context: context,
                callback: callback
            }];
        }
    }

    /**
     * Fire an event.
     *
     * @param {String}    event     the name of the event to fire.
     * @param {...Object} arguments arguments to pass to the callback functions of the event.
     */
    fire(event) {
        var args = _.slice(arguments, 1);

        if (event in this.listeners) {
            for (let listener of this.listeners[event]) {
                listener.callback.apply(listener.context, args);
            }
        }
    }

}

module.exports = SynchronousEvent;