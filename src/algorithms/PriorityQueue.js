function hasAllProperties(o, c) {
    for (var property in c) {
        if (c.hasOwnProperty(property)) {
            if (property in o) {
                if (_.isObject(o[property])) {
                    if (!hasAllProperties(o[property], c[property])) {
                        return false;
                    }
                } else {
                    if (o[property] !== c[property]) {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
    }

    return true;
}

/**
 * A max priority queue.
 */
class PriorityQueue {

    /**
     * Creates the PriorityQueue from scratch or from an existing max heap
     * represented as an array. The behaviour of the PriorityQueue is undefined
     * if the given array is not a max heap.
     *
     * @param  {Array=} heap an max existing heap to use for the PriorityQueue.
     */
    constructor(heap) {
        if (_.isArray(heap)) {
            this.heap = heap;
        } else {
            this.heap = [null];
        }
    }

    /**
     * Private method. Percolates up the element at index i.
     *
     * @param  {Number} i the index of the element to percolateUp.
     */
    percolateUp(i) {
        while (_.floor(i / 2) > 0) {

            if (this.heap[i].p > this.heap[_.floor(i / 2)].p) {
                var tmp = this.heap[_.floor(i / 2)];
                this.heap[_.floor(i / 2)] = this.heap[i];
                this.heap[i] = tmp;
            }

            i = _.floor(i / 2);
        }
    }

    /**
     * Push a new object x into the PriorityQueue with a priority p.
     *
     * @param  {Object} x the object to push into the queue.
     * @param  {Number} p the priority of the object.
     */
    push(x, p) {
        this.heap.push({ x: x, p: p });
        this.percolateUp(this.heap.length - 1);
    }

    /**
     * Private method. Returns the index of the max child of the element at
     * index i.
     *
     * @param  {Number} i the element to find the max child of.
     * @return {Number}   the index of the max child of the element at i.
     */
    maxChild(i) {
        if ((i * 2 + 1) > this.heap.length - 1) {
            return i * 2;
        } else {
            if (this.heap[i * 2].p > this.heap[i * 2 + 1].p) {
                return i * 2;
            } else {
                return i * 2 + 1;
            }
        }
    }

    /**
     * Private method. Percolates down the element at index i.
     *
     * @param  {Number} i the index of the element to percolateUp.
     */
    percolateDown(i) {
        while ((i * 2) <= this.heap.length - 1) {
            var j = this.maxChild(i);

            if (this.heap[i].p < this.heap[j].p) {
                var tmp = this.heap[i];
                this.heap[i] = this.heap[j];
                this.heap[j] = tmp;
            }

            i = j;
        }
    }

    /**
     * Pops the object with the highest priority from the queue, and return it.
     * If no such object exists, returns null.
     *
     * @return {Object} the object with the highest priority in the queue.
     */
    pop() {
        if (this.heap.length < 2) {
            return null;
        }

        var max = this.heap[1];
        this.heap[1] = this.heap[this.heap.length - 1];
        this.heap.pop();
        this.percolateDown(1);
        return max.x;
    }

    /**
     * Returns the object with the highest priority in the queue without
     * removing it from the queue. If no such object exists, returns null.
     *
     * @return {Object} the object with the highest priority in the queue.
     */
    peak() {
        if (this.heap.length < 2) {
            return null;
        }

        return this.heap[1].x;
    }

    /**
     * Search for an object that at least matches all properties of `c`. Not that this means the object can have more
     * properties than `c`.
     *
     * @param {Object} c the criteria to search on.
     * @return {Object} the full object if found, or null otherwise.
     */
    search(c) {
        for (var i = 1; i < this.heap.length; i++) {
            if (hasAllProperties(this.heap[i].x, c)) {
                return this.heap[i];
            }
        }

        return null;
    }

    /**
     * Use only the encapsulated array for JSON.stringify. Easier to persist
     * this in memory this way.
     *
     * @return {Object} the encapsulated.
     */
    toJSON () {
        return this.heap;
    }

    /**
     * The number of elements in the priority queue.
     * @property {Number} length the number of elements in the priority queue.
     */
    get length() {
        return this.heap.length - 1;
    }

}

module.exports = PriorityQueue;