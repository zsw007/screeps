/**
 * Assists in selection of items from a list of options such that the selected items fits into a given ratio limited by
 * a maximum.
 *
 * @example
 * > var options = ['sugar', 'salt', 'pepper', 'cream'];
 * > var current = {};
 * > var ratio = {
 *     sugar: 2,
 *     salt: 3,
 *     pepper: 1,
 *     cream: 1
 * };
 * > var maximum = {
 *     cream: 1
 * };
 * > var ratio = new Ratio(options, current, ratio, maximum);
 * > ratio.next();
 * "sugar"
 * > ratio.next();
 * "salt"
 * > ratio.next();
 * "pepper"
 * > ratio.next();
 * "cream"
 * > ratio.next();
 * "salt"
 * > ratio.next();
 * "sugar"
 * > ratio.next();
 * "salt"
 * > ratio.next();
 * "sugar"
 * > ratio.next();
 * "salt"
 * > ratio.next();
 * "pepper"
 * > ratio.next();
 * "salt"
 */
class Ratio {

    /**
     * Initializes the Ratio object.
     *
     * @param {[String]} options=[] an array of options.
     * @param {Object}   current={} an { option: number } mapping of the number of each option currently selected.
     * @param {Object}   ratio={}   an { option: number } mapping of the desired ratio to select the options by.
     * @param {Object}   maximum={} an { option: number } mapping of the maximum number of each option allowed.
     */
    constructor(options, current, ratio, maximum) {
        this.options = options || [];
        this.current = current || {};
        this.ratio = ratio || {};
        this.maximum = maximum || {};

        for (let option of this.options) {
            // If an option is not in current, then it is equivalent to 0.
            if (!(option in this.current)) {
                this.current[option] = 0;
            }

            // If ratio is not specified for an option, we'll assume it is 1.
            if (!(option in this.ratio)) {
                this.ratio[option] = 1;
            }
        }

        // Prevents division by 0 errors and undefined behaviours. A ratio of 0 is equivalent to not wanting it.
        _.forOwn(this.ratio, (value, key) => {
           if (value <= 0) {
               this.ratio[key] = 1;
               this.maximum[key] = 0;
           }
        });
    }

    /**
     * Adds a new option.
     *
     * @param {String} option  the name of the option.
     * @param {Number} current the current number of items of this option.
     * @param {Number} ratio   the desired ratio to maintain for this option.
     * @param {Number} maximum the maximum limit of their option.
     */
    add(option, current, ratio, maximum) {
        this.options.push(option);
        this.current[option] = current;
        this.ratio[option] = ratio;
        this.ratio[maximum] = maximum;
    }

    /**
     * Selects the next n options, returns a { option: number} mapping representing the number of each options selected,
     * and updates the count of the current number of items selected. If n is not specified, then n is equivalent to 1,
     * and a single option is returned instead.
     */
    next(n) {
        if (_.isFinite(n)) {
            var selections = {};

            for (let i = 0; i < n; i++) {
                let select = this.next();

                if (select in selections) {
                    selections[select]++;
                } else {
                    selections[select] = 1;
                }
            }

            return selections;
        } else {
            var available = _.filter(this.options, (option) => !(option in this.maximum)
                                                              || this.current[option] < this.maximum[option]);
            var select = _.min(available, (option) => this.current[option] / this.ratio[option]);

            this.current[select]++;

            return select;
        }
    }

}

module.exports = Ratio;