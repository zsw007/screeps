var Khala = require('./executors/Khala');
var UnitFactory = require('./units/UnitFactory');
var BuildingFactory = require('./buildings/BuildingFactory');

module.exports.loop = function () {

    PathFinder.use(true);

    // We give a high level command to every unit and structure.
    new Khala().will();

    // Every unit then executes the high level command with lower level actions.
    _.forOwn(Game.creeps, (creep, name) => {
        var unit = UnitFactory.getUnit(creep);
        unit.run();
    });

    // Similarly, every structure executes their high level command with lower level actions.
    _.forOwn(Game.structures, (structure, name) => {
        var building = BuildingFactory.getBuilding(structure);
        building.run();
    });

};