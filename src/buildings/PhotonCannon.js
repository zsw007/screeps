var __ = require('../libs/lodash');
require('../plugins/CreepPlugin');

const MODE_ATTACK = 'attack';

class PhotonCannon extends StructureTower {

    constructor(structure) {
        super(structure.id);
        this.memory.status = this.memory.status || { mode: MODE_ATTACK, target: null };
    }

    /**
     * Calculate the effective amount of damage this tower can do to the targeted Creep based on the amount of healing
     * the creep and nearby creeps offer.
     *
     * @param {Creep} target the creep to attack.
     * @return {Number} the amount of damage done to the target.
     */
    calculateEffectiveDamage(target) {
        if (target instanceof Creep === false) {
            return 0;
        }

        var damage = this.calculateDamage(target);

        // Calculate heals.
        var upcloseHeals = __.sumBy(target.pos.findInRange(FIND_HOSTILE_CREEPS, 1),
            (creep) => creep.getNumBodyparts(HEAL));
        var rangedHeals = __.sumBy(target.pos.findInRange(FIND_HOSTILE_CREEPS, 3),
            (creep) => creep.getNumBodyparts(HEAL));
        rangedHeals -= upcloseHeals;

        var effectiveDamage = damage - (upcloseHeals * HEAL_POWER) - (rangedHeals * RANGED_HEAL_POWER);

        if (effectiveDamage < 0) {
            return 0;
        } else {
            return effectiveDamage;
        }
    }

    /**
     * Calculate the amount of damage this tower can do to the targeted Creep.
     *
     * @param {Creep} target the creep to attack.
     * @return {Number} the amount of damage done to the target.
     */
    calculateDamage(target) {
        if (target instanceof Creep === false) {
            return 0;
        }

        // Since 600 hits at range <= 5 to 150 hits at range >= 20. We can calculate our damage.
        let range = this.pos.getRangeTo(target);

        let damage = TOWER_POWER_ATTACK * (1 - ((range - TOWER_OPTIMAL_RANGE) *
            (TOWER_FALLOFF / (TOWER_FALLOFF_RANGE - TOWER_OPTIMAL_RANGE))));

        if (range <= TOWER_OPTIMAL_RANGE) {
            damage = TOWER_POWER_ATTACK;
        } else if (range >= TOWER_FALLOFF_RANGE) {
            damage = TOWER_POWER_ATTACK * (1 - TOWER_FALLOFF);
        }

        return damage;
    }

    /**
     * Attack the targeted Creep. If there is no target, a target will be selected automatically.
     *
     * @param {Creep=} target the creep to attack.
     */
    attack(target) {

        // Attack a defined target.
        if (target !== undefined) {

            var attack = super.attack(target);

            // If the selected target is invalid, try to automatically select a target instead and attack.
            if (attack === ERR_INVALID_TARGET) {
                return this.attack();
            }

            return attack;
        }

        // Automatically select a target to attack.
        var hostile = this.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (hostile) {
            return super.attack(hostile);
        }

        return ERR_INVALID_TARGET;
    }

    run() {

        if (this.inAttackMode()) {
            var target = Game.getObjectById(this.memory.status.target);
            this.attack(target);
        }

    }

    inAttackMode() {
        return this.memory.status.mode === MODE_ATTACK;
    }

    engageAttackMode(target) {
        if (target !== undefined) {
            if (target === null || target instanceof Creep === false) {
                return ERR_INVALID_TARGET;
            }

            this.memory.status.target = target.id;
        } else {
            this.memory.status.target = null;
        }

        this.memory.status.mode = MODE_ATTACK;

        return OK;
    }

    get memory() {
        Memory.structures = Memory.structures || {};
        Memory.structures[this.id] = Memory.structures[this.id] || {};
        return Memory.structures[this.id];
    }

    set memory(memory) {
        Memory.structures = Memory.structures || {};
        Memory.structures[this.id] = memory;
    }

}

module.exports = PhotonCannon;