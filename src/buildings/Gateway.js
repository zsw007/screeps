var PriorityQueue = require('../algorithms/PriorityQueue');

class Gateway extends StructureSpawn {

    constructor(structure) {
        super(structure.id);
    }

    enqueue(unit, priority) {
        this.queue.push(unit, priority);
    }

    inquire(unit) {
        return this.queue.search(unit);
    }

    run() {
        var unit = this.queue.peak();
        
        if (unit) {
            var canCreateCreep = this.canCreateCreep(unit.body, unit.name);

            if (canCreateCreep === OK) {
                this.createCreep(unit.body, unit.name, unit.memory);
                this.queue.pop();
            } else if (canCreateCreep === ERR_NAME_EXISTS) {
                this.createCreep(unit.body, undefined, unit.memory);
                this.queue.pop();
            } else if (canCreateCreep === ERR_INVALID_ARGS) {
                this.queue.pop();
            } else if (canCreateCreep === ERR_NOT_ENOUGH_ENERGY
                    && this.room.energyCapacityAvailable < Gateway.calculateBodyEnergy(unit.body)) {
                this.queue.pop();
            }
        }
    }

    /**
     * Calculates the amount of energy needed for all the body parts.
     *
     * @param {[Number]} body an array of body parts.
     */
    static calculateBodyEnergy(body) {
        return _.sum(body, (part) => BODYPART_COST[part]);
    }

    get queue() {
        this.memory.queue = this.memory.queue || new PriorityQueue();
        return new PriorityQueue(this.memory.queue);
    }

    set queue(queue) {
        this.memory.queue = queue;
    }

}

module.exports = Gateway;