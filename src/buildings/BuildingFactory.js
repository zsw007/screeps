var Building = require('./Building');
var Gateway = require('./Gateway');
var PhotonCannon = require('./PhotonCannon');

class BuildingFactory {

    /**
     * Gets the Building that associates with the given structure
     * @param {OwnedStructure} structure the structure to get the Building from.
     */
    static getBuilding(structure) {

        if (structure instanceof StructureSpawn) {
            return new Gateway(structure);
        }

        if (structure instanceof StructureTower) {
            return new PhotonCannon(structure);
        }

        return new Building(structure);
    }

}

module.exports = BuildingFactory;