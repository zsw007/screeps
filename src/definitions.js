module.exports = {

    TERRAIN_SWAMP: 'swamp',
    TERRAIN_PLAIN: 'plain',

    /**
     * Returns whether or not the structure is a structure that requires maintenance.
     *
     * @param {Structure} structure the structure to examine.
     */
    maintenanceRequired: function(structure) {
        return structure instanceof Structure
            && ('ticksToDecay' in structure || structure.structureType === STRUCTURE_CONTAINER);
    },

    /**
     * Returns whether or not the `structure` contains energy.
     *
     * @param {Structure} structure the structure to examine.
     */
    hasEnergy: function(structure) {
        return structure instanceof Structure
            && (('energy' in structure && structure.energy > 0)
             || ('store' in structure && RESOURCE_ENERGY in structure.store && structure.store[RESOURCE_ENERGY] > 0))
    },

    /**
     * Returns whether or not the `structure` is a storage structure.
     *
     * A storage structure is either a Storage, Container, Link or Terminal. This is because the primary purpose of
     * these structure is to store energy or mineral, without using them up for any purposes. The ability to remotely
     * transfer resources is an optional benefit of Link and Terminal.
     *
     * @param {Structure} structure the structure to examine.
     */
    isStorage: function(structure) {
        return structure instanceof Structure
            && (structure.structureType === STRUCTURE_CONTAINER
            || (structure.structureType === STRUCTURE_LINK && structure.my)
            || (structure.structureType === STRUCTURE_STORAGE && structure.my)
            || (structure.structureType === STRUCTURE_TERMINAL && structure.my));
    },

    /**
     * Returns whether or not the structure is a structure that allows depositing.
     *
     * @param {Structure} structure the structure to examine.
     */
    isDepot: function(structure) {
        return structure instanceof Structure
            && (structure.structureType === STRUCTURE_SPAWN
            || structure.structureType === STRUCTURE_EXTENSION
            || structure.structureType === STRUCTURE_STORAGE
            || structure.structureType === STRUCTURE_TOWER);
    },

    /**
     * Returns whether the structure is considered an obstacle.
     *
     * @param {Structure} structure the structure to check.
     * @return whether the structure is an obstacle.
     */
    isObstacle: function(structure) {
        return OBSTACLE_OBJECT_TYPES.indexOf(structure.structureType) !== -1
            || (structure.structureType === STRUCTURE_RAMPART && !structure.my);
    },

    /**
     * Determines whether the object returned by a lookAt operation contains an obstacle.
     *
     * @param  {Object} lookAt the object returned by a lookAt operation.
     * @return {Boolean} whether the object is an obstacle.
     */
    isObstacleAt: function(lookAt) {
        return OBSTACLE_OBJECT_TYPES.indexOf(lookAt.type) !== -1
            || OBSTACLE_OBJECT_TYPES.indexOf(lookAt[lookAt.type]) !== -1
            || (lookAt.type === LOOK_STRUCTURES && this.isObstacle(lookAt.structure));
    },
};