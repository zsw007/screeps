var SCV = require('../units/SCV');
var MULE = require('../units/MULE');
var definitions = require('../definitions');
require('../plugins/SourcePlugin');
require('../plugins/RoomPlugin');

class EconomyExecutor {

    constructor(roomExecutor, room) {
        // The upper level executor.
        this.roomExecutor = roomExecutor;

        // The room this executor is responsible for.
        this.room = room;
    }

    will() {
        var units = this.room.units;

        var sources = this.room.find(FIND_SOURCES);
        var mules = _.filter(units, (unit) => MULE.isMULE(unit));
        this.assignSources(mules, sources);

        var scvs = _.filter(units, (unit) => SCV.isSCV(unit));
        this.assignJobs(scvs);

        var storages = this.room.find(FIND_STRUCTURES, {
            filter: (structure) => definitions.isStorage(structure) && definitions.hasEnergy(structure)
        });

        var resources = this.room.find(FIND_DROPPED_RESOURCES, {
            filter: (resource) => resource.resourceType === RESOURCE_ENERGY
        });

        // Assign existing scvs to storages.
        this.assignStorages(scvs, storages, resources);

        // Make new scvs as needed.
        this.spawnSCVs(scvs, storages, resources);
    }


    spawnSCVs(scvs, storages, resources) {
        // The effective energy of all the storages and resources.
        var effectiveEnergy = {};

        for (let storage of storages) {
            if ('energy' in storage) {
                effectiveEnergy[storage.id] = storage.energy;
            } else {
                effectiveEnergy[storage.id] = storage.store[RESOURCE_ENERGY];
            }
        }

        for (let resource of resources) {
            effectiveEnergy[resource.id] = resource.amount;
        }

        for (let id in effectiveEnergy) {
            if (effectiveEnergy[id] > CARRY_CAPACITY && !this.roomExecutor.waitSCV(Game.getObjectById(id))) {
                this.roomExecutor.requestSCV(1, 1, 1, Game.getObjectById(id));
            }
        }
    }

    assignStorages(scvs, storages, resources) {
        // The effective energy of all the storages and resources.
        var effectiveEnergy = {};

        for (let storage of storages) {
            if ('energy' in storage) {
                effectiveEnergy[storage.id] = storage.energy;
            } else {
                effectiveEnergy[storage.id] = storage.store[RESOURCE_ENERGY];
            }
        }

        for (let resource of resources) {
            effectiveEnergy[resource.id] = resource.amount;
        }

        for (let scv of scvs) {
            if (scv.hasEnergySource()) {
                var assigned = scv.memory.job.energy.source.id || scv.memory.job.energy.storage || scv.memory.job.energy.resource;
                if (assigned in effectiveEnergy) {
                    effectiveEnergy[assigned] -= scv.getActiveBodyparts(CARRY) * CARRY_CAPACITY;
                }
            } else {
                var assign = _.max(_.keys(effectiveEnergy), (key) => effectiveEnergy[key]);
                scv.assignResource(Game.getObjectById(assign));
                effectiveEnergy[assign] -= scv.getActiveBodyparts(CARRY) * CARRY_CAPACITY;
            }
        }
    }

    assignSources(mules, sources) {
        var idleMules = _.filter(mules, (mule) => mule.source === null);

        for (let source of sources) {
            let assignedMules = _.filter(mules, (mule) => mule.source === source);
            let saturationRemaining = source.maxSaturation - assignedMules.length;
            let workNeeded = source.maxWorkers - _.sum(assignedMules, (mule) => mule.getActiveBodyparts(WORK));

            // Check if we need more mules, and we can assign more mules.
            if (workNeeded > 0 && saturationRemaining > 0) {
                let move = 1;
                let work = 5;
                let carry = 0;

                // Check whether we have a storage.
                let storages = source.storages;
                let storage = null;
                if (storages.length > 0) {
                    storage = storages[0];
                    carry = 1;
                }

                if (idleMules.length > 0) {
                    idleMules[0].assignSource(source);
                    idleMules[0].assignStorage(storage);
                } else if (!this.roomExecutor.waitMULE(source)) {
                    this.roomExecutor.requestMULE(move, work, carry, source, storage);
                }
            }

            // Assign any mule harvesting on this source without a storage.
            let mulesNoStorage = _.filter(assignedMules, (mule) => mule.storage === null);
            for (let mule of mulesNoStorage) {
                // Check whether we have a storage.
                let storages = source.storages;
                if (storages.length > 0) {
                    mule.assignStorage(storages[0]);
                }
            }
        }
    }

    nextJob(stats) {
        if (this.room.energy < this.room.energyCapacity && stats.depositer === 0) {
            return 'depositer';
        }

        // We should have at least one upgrader to make sure we don't get downgraded.
        if (stats.upgrader === 0) {
            return 'upgrader';
        }

        // We should have one maintainer if maintenance is required.
        if (this.room.maintenanceRequired() && stats.maintainer === 0) {
            return 'maintainer';
        }

        // Now we take all the rows that needs assigning.
        var assignable = ['upgrader'];

        if (this.room.energy < this.room.energyCapacity) {
            assignable.push('depositer');
        }

        if (this.room.repairRequired()) {
            assignable.push('repairer');
        }

        if (this.room.constructionSites.length > 0) {
            assignable.push('builder');
        }

        return _.min(assignable, (job) => stats[job]);
    }

    assignJobs(scvs) {
        var stats = _.transform(scvs, (result, unit) => {
            result.builder = result.builder || 0;
            result.depositer = result.depositer || 0;
            result.upgrader = result.upgrader || 0;
            result.maintainer = result.maintainer || 0;
            result.repairer = result.repairer || 0;

            if (unit.isBuilder()) {
                result.builder++;
            } else if (unit.isDepositer()) {
                result.depositer++;
            } else if (unit.isUpgrader()) {
                result.upgrader++;
            } else if (unit.isMaintainer()) {
                result.maintainer++;
            } else if (unit.isRepairer()) {
                result.repairer++;
            }
        }, {});

        for (let scv of scvs) {
            if (scv.isIdle()) {
                let assign = this.nextJob(stats);

                switch(assign) {
                    case 'depositer':
                        scv.assignDepositer();
                        stats.depositers++;
                        return OK;
                    case 'upgrader':
                        scv.assignUpgrader();
                        stats.upgraders++;
                        return OK;
                    case 'maintainer':
                        scv.assignMaintainer();
                        stats.maintainers++;
                        return OK;
                    case 'builder':
                        scv.assignBuilder();
                        stats.builders++;
                        return OK;
                    case 'repairer':
                        scv.assignRepairer();
                        stats.repairers++;
                        return OK;
                    default:
                        scv.assignUpgrader();
                        stats.upgraders++;
                        return OK;
                }
            }
        }
    }

    get name() {
        return this.room.name;
    }

}

module.exports = EconomyExecutor;