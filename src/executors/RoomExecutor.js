var EconomyExecutor = require('./EconomyExecutor');
var ProductionExecutor = require('./ProductionExecutor');
var DefenseExecutor = require('./DefenseExecutor');
var definitions = require('../definitions');

/**
 * Each Room is managed by a RoomExecutor, it passes command down to other Executors to manage the room.
 */
class RoomExecutor {

    constructor(khala, room) {
        this.khala = khala;
        this.room = room;

        this.economyExecutor = new EconomyExecutor(this, room);
        this.productionExecutor = new ProductionExecutor(this, room);
        this.defenseExecutor = new DefenseExecutor(this, room);
    }

    will() {
        // If we own this room.
        if ('controller' in this.room && this.room.controller.my) {
            this.economyExecutor.will();
            this.productionExecutor.will();
            this.defenseExecutor.will();
        }
    }

    /**
     * Request a MULE to be spawned with the ratio of MOVE : WORK : CARRY body parts to be `move` : `work` : `carry`,
     * and direct it to harvest from `source` and deposit into `storage`.
     *
     * @param {Number}     move    the ratio of the number of MOVE body parts.
     * @param {Number}     work    the ratio of the number of WORK body parts.
     * @param {Number}     carry   the ratio of the number of CARRY body parts.
     * @param {Source}     source  the source to harvest from.
     * @param {Structure=} storage the storage to deposit into, null if undefined or invalid.
     */
    requestMULE(move, work, carry, source, storage) {
        if (!definitions.isStorage(storage)) {
            storage = null;
        }

        if (!(source instanceof Source)) {
            return;
        }

        this.productionExecutor.queueMULE(move, work, carry, source, storage, 2);
    }

    /**
     * Returns whether or not to keep waiting for the requested MULE to spawn.
     *
     * @param  {Source} source the source to check the request for
     * @return {Boolean} true if it is necessary to keep waiting, and a new request should not be made; otherwise false.
     */
    waitMULE(source) {
        return this.productionExecutor.waitMULE(source.pos);
    }

    /**
     * Request a SCV to be spawned with `work` number of WORK body parts, `carry` number of CARRY body parts, `move`
     * number of MOVE body parts from the spawn closest to `source`.
     *
     * @param {Number}   work the number of WORK body parts.
     * @param {Number}   carry the number of CARRY body parts.
     * @param {Number}   move the number of MOVE body parts.
     * @param {RoomObject} target the source to determine which spawn to spawn request from.
     */
    requestSCV(work, carry, move, target) {
        this.productionExecutor.queueSCV(work, carry, move, target, 1);
    }

    /**
     * Inquire whether the requested worker for `source` is still being processed. Returns true if it is still
     * being processed, that is, another worker should not be request. Returns false if it has finished processing, or
     * was somehow forgotten. That is, it should request again if needed.
     *
     * @param {Source} source the source to check the request for
     * @return {Boolean} true if another request should not be made, false if another request can be made.
     */
    waitSCV(source) {
        return this.productionExecutor.waitSCV(source.pos);
    }

}

module.exports = RoomExecutor;