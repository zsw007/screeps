var RoomExecutor = require('./RoomExecutor');

/**
 * The Khala unifies activity in all rooms which we have units in, and provide an overriding general direction to all of
 * them.
 */
class Khala {

    constructor() {

        // Room Executors for each room. Note that Game.room only contains rooms we have vision of.
        this.roomExecutors = {};

        _.forOwn(Game.rooms, (room, name) => {
            this.roomExecutors[name] = new RoomExecutor(this, room);
        });

    }

    /**
     * As the Khala wills.
     */
    will() {
        _.forOwn(Memory.creeps, (memory, name) => {
            if (!Game.creeps[name]) {
                //this.revive(memory);
                delete Memory.creeps[name];
            }
        });

        _.forOwn(this.roomExecutors, (executor, name) => {
            executor.will();
        });
    }

    /**
     * Revive those who are one with the Khala.
     */
    revive(memory) {
        if (memory.from && memory.from in this.roomExecutors) {
            this.roomExecutors[memory.from].notifyUnitExpired(memory);
        }
    }

}

module.exports = Khala;