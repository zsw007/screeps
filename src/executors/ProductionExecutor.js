var __ = require('../libs/lodash');
var UnitTypes = require('../units/UnitTypes');
var UnitFactory = require('../units/UnitFactory');
var Gateway = require('../buildings/Gateway');

/**
 * Each Hatchery is managed by a ProductionExecutor. This Executor manages what to gateway from each Hatchery based upon
 * orders from above.
 */
class ProductionExecutor {

    constructor(roomExecutor, room) {
        this.roomExecutor = roomExecutor;
        this.room = room;

        // The gateways this executor is responsible for.
        this.gateways = _.map(this.room.find(FIND_MY_SPAWNS), (spawn) => new Gateway(spawn));
    }

    /**
     * Return the closest gateway to the given position.
     *
     * @param {RoomPosition} pos the position to find the closest gateway to.
     * @return {Gateway} the closest gateway to `pos`.
     */
    closestGateway(pos) {
        return __.minBy(this.gateways, (gateway) => gateway.pos.getRangeTo(pos));
    }

    /**
     * Queue up a MULE to be spawned with the ratio of MOVE : WORK : CARRY body parts to be `move` : `work` : `carry`,
     * and direct it to harvest from `source` and deposit into `storage`.
     *
     * @param {Number}     move     the ratio of the number of MOVE body parts.
     * @param {Number}     work     the ratio of the number of WORK body parts.
     * @param {Number}     carry    the ratio of the number of CARRY body parts.
     * @param {Source}     source   the source to harvest from.
     * @param {Structure=} storage  the storage to deposit into, null if undefined or invalid.
     * @param {Number}     priority the priority of the MULE.
     */
    queueMULE(move, work, carry, source, storage, priority) {
        // Make sure our lowest ratio is 1.
        var lowestRatio = Math.min(move, work, carry) || 1;
        move = _.ceil(move / lowestRatio);
        work = _.ceil(work / lowestRatio);
        carry = _.ceil(carry / lowestRatio);

        var ratios = {};
        ratios[MOVE] = move;
        ratios[WORK] = work;
        ratios[CARRY] = carry;

        // The ratio is also the maximum since we don't really need more parts than this.
        var bodyparts = ProductionExecutor.makeBodyparts(this.room.energyCapacityAvailable, ratios, ratios);
        if (!bodyparts) {
            return;
        }

        var body = [];

        // Distribute the MOVE first, since our MULE mostly do not need to move. Then distribute the CARRY since we can
        // drop the harvested energy to the ground. Finally distribute the WORK since that's mostly all we do.
        while(bodyparts[MOVE] > 0) {
            body.push(MOVE);
            bodyparts[MOVE]--;
        }

        while(bodyparts[CARRY] > 0) {
            body.push(CARRY);
            bodyparts[CARRY]--;
        }

        while(bodyparts[WORK] > 0) {
            body.push(WORK);
            bodyparts[WORK]--;
        }

        var MULE = UnitFactory.makeMULE(body, source, storage);
        if (this.gateways.length > 0) {
            this.closestGateway(MULE.memory.goto).enqueue(MULE, priority);
        }
    }

    /**
     * Returns whether or not to keep waiting for the requested MULE to spawn.
     *
     * @param {RoomPosition} goto the position the MULE is designated towards.
     * @return {Boolean} true if it is necessary to keep waiting, and a new request should not be made; otherwise false.
     */
    waitMULE(goto) {
        var searchFor = {
            memory: {
                unit: UnitTypes.MULE,
                goto: goto,
            }
        };

        return this.inquireUnit(searchFor);
    }

    /**
     * Queue up a SCV to be spawned with `work` number of WORK body parts, `carry` number of CARRY body
     * parts, `move` number of MOVE body parts, designated towards the room indicated by `roomPos`.
     *
     * @param {Number}       work  the number of WORK body parts.
     * @param {Number}       carry the number of CARRY body parts.
     * @param {Number}       move  the number of MOVE body parts.
     * @param {RoomObject}   target  the position of the room to move the scv to.
     * @param {Number}             priority the priority with which to queue the scv.
     */
    queueSCV(work, carry, move, target, priority) {
        // Make sure our lowest ratio is 1.
        var lowestRatio = Math.min(move, work, carry) || 1;
        move = _.ceil(move / lowestRatio);
        work = _.ceil(work / lowestRatio);
        carry = _.ceil(carry / lowestRatio);

        var ratios = {};
        ratios[MOVE] = move;
        ratios[WORK] = work;
        ratios[CARRY] = carry;

        var maximum = {};
        maximum[WORK] = 1;

        // The ratio is also the maximum since we don't really need more parts than this.
        var bodyparts = ProductionExecutor.makeBodyparts(this.room.energyCapacityAvailable, ratios, maximum);
        if (!bodyparts) {
            return;
        }

        var body = [];

        // Distribute body parts as MOVE, WORK, CARRY alternating. We put MOVE first because we can still potentially
        // repair something with WORK even if we can't move. But WORK is useless without CARRY, so we put WORK before
        // CARRY.
        while(bodyparts[MOVE] + bodyparts[CARRY] > 0) {
            if (bodyparts[MOVE] > 0) {
                body.push(MOVE);
                bodyparts[MOVE]--;
            }

            if (bodyparts[CARRY]) {
                body.push(CARRY);
                bodyparts[CARRY]--;
            }
        }

        while (bodyparts[WORK] > 0) {
            body.push(WORK);
            bodyparts[WORK]--;
        }

        var scv = UnitFactory.makeSCV(body, target);
        if (this.gateways.length > 0) {
            this.closestGateway(scv.memory.goto).enqueue(scv, priority);
        }
    }

    /**
     * Inquire whether the requested worker designated for `goto` is still being processed. Returns true if it is still
     * being processed, that is, another worker should not be request. Returns false if it has finished processing, or
     * was somehow forgotten. That is, it should request again if needed.
     *
     * @param {RoomPosition} goto the position the worker is designated towards.
     * @return {Boolean} true if another request should not be made, false if another request can be made.
     */
    waitSCV(goto) {

        var searchFor = {
            memory: {
                unit: UnitTypes.SCV,
                goto: goto,
            }
        };

        return this.inquireUnit(searchFor);
    }

    /**
     * Inquire whether the requested unit is still in queue. Returns true if it is still being processed.
     * Returns false if it has finished processing, or was somehow forgotten.
     *
     * @param {Object} unit as much of the unit as necessary to search for.
     * @return {boolean} whether the unit is in queue.
     */
    inquireUnit(unit) {
        for (var gateway of this.gateways) {
            // TODO: This is a temporary solution for the issue of the unit being immediately queued up as soon as it is popped from the queue. Need to detect this and wait until it spawns and then reevaluate.
            if (gateway.inquire(unit) || gateway.spawning) {
                return true;
            }
        }

        return false;
    }

    will() {

    }

    /**
     * Returns the amount of each body parts that can be spawned following the given `ratio` using as much `energy` as
     * possible, but not using more body parts than the `maximum`. At least one of each part in the `ratio` is used. If
     * this is not possible, null is returned.
     *
     * The `ratios`/`maximum` parameter is an object such that:
     *     - Each property should be named after a body part.
     *     - Each property should be a number representing the ratio/maximum of that body part.
     *
     * Example usage:
     * > var energy = 400;
     * > var ratios = {};
     * > var ratios[WORK] = 2;
     * > var ratios[CARRY] = 1;
     * > var ratios[MOVE] = 2;
     * > makeBodyparts(energy, ratios);
     * {
     *     WORK : 2,
     *     CARRY : 1,
     *     MOVE : 1
     * }
     *
     * @param  {Number}  energy  the amount of available energy.
     * @param  {Object}  ratios  ratio of body parts.
     * @param  {Object=} maximum the maximum amount of each body part.
     * @return {Object} amount of each body part that can be spawned, or null if not all requested parts can be used.
     */
    static makeBodyparts(energy, ratios, maximum) {
        if (!maximum) {
            maximum = {};
        }

        // All the parts that are required.
        var requiredParts = _.filter(_.keys(ratios), (parts) => ratios[parts] > 0);

        // Check if we are able to include every part at least once.
        if (_.sum(requiredParts, (part) => BODYPART_COST[part]) > energy) {
            return null;
        }

        // The minimal amount of energy any one part needs.
        var minEnergy = _.min(_.map(requiredParts, (part) => BODYPART_COST[part]));

        var bodyparts = {};
        var totalParts = 0;

        // Used to follow the ratio.
        var tmpRatio = Object.assign({}, ratios);

        var added = true;
        while (added && energy > minEnergy && totalParts < MAX_CREEP_SIZE) {
            // When we stop adding, we know that we can't add anymore.
            added = false;

            // To follow the ratio, we decrease each part as it is selected. Then once we have used up all the parts in
            // the ratio, we renew the ratio and continue until we run out of energy.
            if (_.reduce(tmpRatio, (result, value, key) => result + value, 0) === 0) {
                tmpRatio = Object.assign({}, ratios);
            }

            for (let part in tmpRatio) {
                if (tmpRatio.hasOwnProperty(part)) {
                    if (part in bodyparts && part in maximum && bodyparts[part] >= maximum[part]) {
                        tmpRatio[part] = 0;
                        continue;
                    }

                    if (tmpRatio[part] > 0 && energy - BODYPART_COST[part] >= 0) {
                        if (bodyparts[part]) {
                            bodyparts[part]++;
                        } else {
                            bodyparts[part] = 1;
                        }

                        tmpRatio[part]--;

                        energy -= BODYPART_COST[part];

                        totalParts++;

                        added = true;
                    }

                    if (totalParts >= MAX_CREEP_SIZE) {
                        added = false;
                        break;
                    }
                }
            }
        }

        return bodyparts;
    }

    get pos() {
        return this.gateway.pos;
    }

    get name() {
        return this.gateway.name;
    }

}

module.exports = ProductionExecutor;