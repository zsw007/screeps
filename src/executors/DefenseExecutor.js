var PhotonCannon = require('../buildings/PhotonCannon');
var __ = require('../libs/lodash');

/**
 * The DefenseExecutor manages the defense of the room. It coordinate local defense efforts with existing forces,
 */
class DefenseExecutor {

    constructor(roomExecutor, room) {
        this.roomExecutor = roomExecutor;
        this.room = room;
    }

    hostileScan(photonCannons) {
        // Scan for hostiles.
        var hostiles = this.room.find(FIND_HOSTILE_CREEPS);

        if (hostiles.length === 0) {
            return OK;
        }

        // Select a single highest priority hostile for maximum efficiency.
        var hostile = __.maxBy(hostiles, (creep) => {

            // We will mathematically evaluate the priority of each target and pick the highest priority.
            var priority = 0;


            var hostilesInRangeThree = creep.pos.findInRange(FIND_HOSTILE_CREEPS, 3).length;
            var myStuffInRangeThree = creep.pos.findInRange(FIND_MY_CREEPS, 3).length
                                    + creep.pos.findInRange(FIND_MY_STRUCTURES, 3).length;
            var myStuffInRangeOne = creep.pos.findInRange(FIND_MY_CREEPS, 1).length
                                  + creep.pos.findInRange(FIND_MY_STRUCTURES, 1).length;

            // The number of ticks this creep will fatigue for. If the creep is fatiguing, then they are less of a
            // priority if there is nothing around them that can make use of their abilities anyway.
            //var fatigueTicks = 1;
            // (creep.getActiveBodyparts(MOVE) > 0) {
            //    fatigueTicks = _.ceil(creep.fatigue / (creep.getActiveBodyparts(MOVE) * 2));
            //}

            //if (fatigueTicks === 0) {
            //    fatigueTicks = 1;
            //}

            // The total amount of effective damage we can do onto this hostile creep.
            //var totalMinDamage = photonCannons.length * (TOWER_POWER_ATTACK * (1 - TOWER_FALLOFF));
            var totalDamage = 0;
            for (let photonCannon of photonCannons) {
                totalDamage += photonCannon.calculateEffectiveDamage(creep);
            }

            // Tracks how many parts have been stacked so far, since we can only attack parts in the order they are
            // placed.
            var effectiveHp = 0;

            for (let part of creep.body) {

                if (totalDamage > part.hits) {
                    totalDamage -= part.hits;
                } else {
                    totalDamage = 0;
                    effectiveHp += part.hits - totalDamage;
                }


                let weight = 1;

                // We weigh each part by what it can accomplish.
                if (part.type === HEAL) {
                    weight = 1.3 + (hostilesInRangeThree / 100);
                } else if (part.type === RANGED_ATTACK) {
                    weight = 1.2 + (myStuffInRangeThree / 100);
                } else if (part.type === ATTACK) {
                    weight = 1 + (myStuffInRangeOne / 100);
                } else if (part.type === TOUGH) {
                    weight = 0.5;
                }


                let effectiveBoost = 1;

                if (part.boost && part.type in BOOSTS && part.boost in BOOSTS[part.type]) {

                    // Boosted parts boost different actions. For our purpose, we don't care which action is
                    // boosted, since all actions are boosted at the same rate. We will just take any arbitrary value
                    // as the boost amount.
                    effectiveBoost = BOOSTS[part.type][part.boost][Object.keys(BOOSTS[part.type][part.boost])[0]];

                    // For damage reduction boosts.
                    if (effectiveBoost < 1) {
                        effectiveBoost = 1 / effectiveBoost;
                    }

                }

                // The more effective the creep is, the more important it is to kill it first. But we also want to
                // actually target creeps we can kill first.
                priority += effectiveBoost * weight;

                // We want to kill the easiest to kill creeps first. (least parts).
                priority -= (effectiveHp / 100) * (1 / weight);
            }

            //console.log('The threat of ' + creep.id + ' is ' + priority);

            return priority;
        });


        var hostileHp = hostile.hits;

        for (let photonCannon of photonCannons) {
            if (hostileHp > 0) {
                photonCannon.engageAttackMode(hostile);
                hostileHp -= photonCannon.calculateEffectiveDamage(hostile);
            } else {
                photonCannon.engageAttackMode();
            }
        }

        return ERR_BUSY;
    }


    will() {

        var towers = this.room.find(FIND_MY_STRUCTURES, {
                filter: (structure) => structure.structureType === STRUCTURE_TOWER
            });
        var photonCannons = _.map(towers, (tower) => new PhotonCannon(tower));


        if (this.hostileScan(photonCannons) !== OK) {
            return;
        }


    }

}

module.exports = DefenseExecutor;