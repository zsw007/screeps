var UnitFactory = require('../units/UnitFactory');
var Definitions = require('../definitions');

Room.prototype.maintenanceRequired = function () {
    return this.find(FIND_STRUCTURES, {
        filter: (structure) => Definitions.maintenanceRequired(structure)
    }).length > 0;
};

Room.prototype.repairRequired = function () {
    return this.find(FIND_MY_STRUCTURES, {
            filter: (structure) => structure.hits < structure.hitsMax
        }).length > 0;
};

/**
 * An array of creeps we own in the room .
 */
Object.defineProperty(Room.prototype, 'creeps', {
    get: function () {
        return this.find(FIND_MY_CREEPS);
    }
});

/**
 * An array of all the creeps in the room.
 */
Object.defineProperty(Room.prototype, 'allCreeps', {
    get: function () {
        return this.find(FIND_CREEPS);
    }
});

/**
 * An array of all the hostile creeps in the room.
 */
Object.defineProperty(Room.prototype, 'hostileCreeps', {
    get: function () {
        return this.find(FIND_HOSTILE_CREEPS);
    }
});

/**
 * An array of creeps we own in the room, defined as their equivalent unit.
 */
Object.defineProperty(Room.prototype, 'units', {
    get: function () {
        return _.map(this.creeps, (creep) =>  UnitFactory.getUnit(creep));
    }
});

/**
 * The total amount of energy stored in all depots in the room. Differs from energyAvailable as this includes all
 * depots for energy, not only spawns and extensions.
 */
Object.defineProperty(Room.prototype, 'energy', {
    get: function () {
        var depots = this.find(FIND_STRUCTURES, {
                        filter: function (structure) {
                            return Definitions.isDepot(structure);
                        }
                    });

        return _.map(depots, (depot) => depot.energy).reduce((a, b) => a + b, 0);
    }
});

/**
 * The maximum amount of energy that can be stored in all depots in the room. Differs from energyCapacityAvailable as
 * this includes all depots for energy, not only spawns and extensions.
 */
Object.defineProperty(Room.prototype, 'energyCapacity', {
    get: function () {
        var depots = this.find(FIND_STRUCTURES, {
            filter: function (structure) {
                return Definitions.isDepot(structure);
            }
        });

        return _.map(depots, (depot) => depot.energyCapacity).reduce((a, b) => a + b, 0);
    }
});

/**
 * An array of construction sites we own in the room.
 */
Object.defineProperty(Room.prototype, 'constructionSites', {
    get: function () {
        return this.find(FIND_MY_CONSTRUCTION_SITES);
    }
});