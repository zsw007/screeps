var definitions = require('../definitions');
require('./RoomPositionPlugin');

/**
 * Returns the number of creeps within `range` distance from this source.
 *
 * @param {Number} range the range to be considered nearby.
 * @return {Number} the number of creeps nearby.
 */
Source.prototype.nearbyCreeps = function(range) {
    var x = this.pos.x;
    var y = this.pos.y;

    var top = y - range;
    var left = x - range;
    var bottom = y + range;
    var right = x + range;

    if (top < 0) {
        top = 0;
    }

    if (left < 0) {
        left = 0;
    }

    if (bottom > 49) {
        bottom = 49;
    }

    if (right > 49) {
        right = 49;
    }

    return this.room.lookForAtArea(LOOK_CREEPS, top, left, bottom, right, true);
};

/**
 * Returns an array of storages associated with this Source. A storage is associated with this Source if it is within a
 * range of 2 from this Source, as well as having the property such that a Creep can both harvest from this Source and
 * transfer into the storage without having to move.
 *
 * @type {[Structure]}
 */
Object.defineProperty(Source.prototype, 'storages', {
    get: function () {
        var x = this.pos.x;
        var y = this.pos.y;

        var top = y - 2;
        var left = x - 2;
        var bottom = y + 2;
        var right = x + 2;

        if (top < 0) {
            top = 0;
        }

        if (left < 0) {
            left = 0;
        }

        if (bottom > 49) {
            bottom = 49;
        }

        if (right > 49) {
            right = 49;
        }

        var storages = [];

        var structures = this.room.lookForAtArea(LOOK_STRUCTURES, top, left, bottom, right, true);
        for (let lookAt of structures) {
            let structure = lookAt.structure;

            // Being in range 2 is implied since we only looking at area within a range of 2.
            if (definitions.isStorage(structure) && structure.pos.inStepTo(this, 1)) {
                storages.push(structure);
            }
        }

        return storages;
    },
});

/**
 * Returns the number of WORK body parts currently harvesting on this Source. A Creep is considered harvesting as long
 * as it is right next to the Source, even if it isn't using the harvest() action.
 *
 * Unlike `saturation`, `workers` counts the number of WORK body parts.
 *
 * @type {Number}
 */
Object.defineProperty(Source.prototype, 'workers', {
    get: function () {
        var workers = 0;
        var lookAtCreeps = this.nearbyCreeps(1);

        for (let lookAt of lookAtCreeps) {
            var creep = lookAt[LOOK_CREEPS];
            workers += creep.getActiveBodyParts(WORK);
        }

        return workers;
    },
});

/**
 * Returns the maximum number of WORK body parts that should be harvesting this Source at all times. Exceeding this will
 * result in the Source depleting before it replenishes.
 *
 * @type {Number}
 */
Object.defineProperty(Source.prototype, 'maxWorkers', {
    get: function () {
        return this.energyCapacity / HARVEST_POWER / ENERGY_REGEN_TIME;
    },
});


/**
 * Returns the number of Creeps currently harvesting on this Source. A Creep is considered harvesting as long as it is
 * right next to the Source, even if it isn't using the harvest() action.
 *
 * Unlike `workers`, `saturation` counts the number of Creeps.
 *
 * @type {Number}
 */
Object.defineProperty(Source.prototype, 'saturation', {
    get: function () {
        return this.nearbyCreeps(1).length;
    },
});

/**
 * Returns the maximum number of Creeps that can harvest from this Source at any given time.
 *
 * @type {Number}
 */
Object.defineProperty(Source.prototype, 'maxSaturation', {
    get: function () {
        var x = this.pos.x;
        var y = this.pos.y;
        var maxSaturation = 0;

        // Check the surrounding 8 cell for spots that a worker can be in.
        var top = y - 1;
        var left = x - 1;
        var bottom = y + 1;
        var right = x + 1;

        if (top < 0) {
            top = 0;
        }

        if (left < 0) {
            left = 0;
        }

        if (bottom > 49) {
            bottom = 49;
        }

        if (right > 49) {
            right = 49;
        }


        var surrounding = this.room.lookAtArea(top, left, bottom, right, true);
        for (var cell of surrounding) {
            // No need to check the source cell itself, we already know it is an obstacle.
            if (cell.x === x && cell.y === y) {
                continue;
            }

            // If it's a flat walkable terrain.
            if (cell.type === LOOK_TERRAIN && OBSTACLE_OBJECT_TYPES.indexOf(cell.terrain) === -1) {
                maxSaturation++;
            }

            // If there is a structure on the terrain that is not walkable. We don't care about creeps since those can
            // move.
            if (cell.type === LOOK_STRUCTURES && OBSTACLE_OBJECT_TYPES.indexOf(cell.structure.structureType) !== -1) {
                maxSaturation--;
            }
        }

        return maxSaturation;
    },
});