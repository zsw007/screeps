/**
 * Get the quantity of body parts of the given type, including fully damage body parts. Boosted body parts will increase
 * the number by the percentage they are boosted by.
 *
 * @param  {Number} type one of the body part constants.
 * @return {Number} the quantity of body parts of the given type.
 */
Creep.prototype.getNumBodyparts = function(type) {

    var num = 0;

    for (let part of this.body) {
        if (part.type !== type) {
            continue;
        }

        if (part.boost && part.type in BOOSTS && part.boost in BOOSTS[part.type]) {

            // Boosted parts boost different actions. For our purpose, we don't care which action is boosted, since
            // all actions are boosted at the same rate. We will just take any arbitrary one as the boost amount.
            let boostAmount = BOOSTS[part.type][part.boost][Object.keys(BOOSTS[part.type][part.boost])[0]];

            // For damage reduction boosts.
            if (boostAmount < 1) {
                boostAmount = _.ceil(1 / boostAmount, 2);
            }

            num += 1 + (boostAmount - 1);

        } else {
            num++;
        }
    }

    return num;

};


Object.defineProperty(Source.prototype, 'bodyStats', {
    get: function () {
        var stats = {};

        return stats;
    },
});