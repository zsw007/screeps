var Definitions = require('../definitions');

/**
 * Returns whether or not it is possible to take one step in any movable direction (no obstacles) from the current
 * position and be within a given range to the target position.
 *
 * @param  {Number|RoomPosition|RoomObject|Object} a either the X position, a RoomPosition, a RoomObject, or an Object
 *                                                   equivalence of a RoomPosition, of the target.
 * @param  {Number}                                b the Y position if a is the X position, otherwise the range.
 * @param  {Number=}                               c the range if b is the Y position, otherwise undefined.
 * @return {Boolean} whether or not taking one step puts us within range of the target position.
 */
RoomPosition.prototype.inStepTo = function (a, b, c) {

    var target;
    var range;
    if (_.isNumber(a) && _.isNumber(b)) {
        target = new RoomPosition(a, b, this.roomName);
        range = c;
    } else if ('pos' in a) {
        target = a.pos;
        range = b;
    } else if (a instanceof RoomPosition) {
        target = a;
        range = b;
    } else if (_.isObject(a) && 'x' in a && 'y' in a && 'roomName' in a) {
        target = new RoomPosition(a.x, a.y, a.roomName);
        range = b;
    } else {
        return false;
    }

    for (let direction of [TOP, TOP_RIGHT, RIGHT, BOTTOM_RIGHT, BOTTOM, BOTTOM_LEFT, LEFT, TOP_LEFT]) {

        var newX = this.x;
        var newY = this.y;

        switch (direction) {
            case TOP:
                newY--;
                break;
            case TOP_RIGHT:
                newY--;
                newX++;
                break;
            case RIGHT:
                newX++;
                break;
            case BOTTOM_RIGHT:
                newY++;
                newX++;
                break;
            case BOTTOM:
                newY++;
                break;
            case BOTTOM_LEFT:
                newY++;
                newX--;
                break;
            case LEFT:
                newX--;
                break;
            case TOP_LEFT:
                newY--;
                newX--;
                break;
        }

        if (newX < 0) {
            newX = 0;
        }

        if (newX > 49) {
            newX = 49;
        }

        if (newY < 0) {
            newY = 0;
        }

        if (newY > 49) {
            newY = 49;
        }

        var newPos = new RoomPosition(newX, newY, this.roomName);

        // Check to see whether the direction is actually movable.
        var isMovable = true;
        var look = newPos.look();
        for (let obj of look) {
            if (Definitions.isObstacleAt(obj)) {
                isMovable = false;
                break;
            }
        }

        // If it's movable, it must also be in range.
        if (isMovable && newPos.inRangeTo(target, range)) {
            return true;
        }

    }

    return false;

};