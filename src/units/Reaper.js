var Unit = require('./Unit');
var UnitTypes = require('./UnitTypes');

class Reaper extends Unit {


    run() {

        // First move to desired room.
        if (this.room.name !== 'W54N53') {
            var destination = new RoomPosition(9, 12, 'W54N53');
            super.moveTo(destination);
        } else {
            // Kill any defending creeps.
            var defenders = this.room.find(FIND_HOSTILE_CREEPS, {
                filter: (creep) => {
                    var spawn = new RoomPosition(9, 12, 'W54N53');
                    if (!spawn.inRangeTo(creep, 7)) {
                        return false;
                    }

                    var body = creep.body;
                    for (var i in body) {
                        var part = body[i];
                        if (part.type === ATTACK && part.hits > 0) {
                            return true;
                        } else if (part.type === WORK && creep.carry.energy > 0 && part.hits > 0) {
                            return true;
                        }
                    }



                    return false;
                }
            });

            if (defenders.length > 0) {
                if(super.attack(defenders[0]) === ERR_NOT_IN_RANGE) {
                    super.moveTo(defenders[0], {
                        ignoreRoads: true,
                    });
                    super.heal(this);
                }
                super.say('glhf', true);
            } else {

                // Kill the spawn.
                var spawns = this.room.find(FIND_HOSTILE_SPAWNS);
                if (spawns.length > 0) {
                    if(super.attack(spawns[0]) === ERR_NOT_IN_RANGE) {
                        super.moveTo(spawns[0], {
                            ignoreRoads: true,
                        });
                        super.heal(this);
                    } else {
                        super.say('gg', true);
                    }

                } else {

                    // Kill anything.
                    var targets = this.room.find(FIND_HOSTILE_CREEPS);
                    if (targets.length > 0) {
                        if (super.attack(targets[0]) === ERR_NOT_IN_RANGE) {
                            super.moveTo(targets[0], {
                                ignoreRoads: true,
                            });
                            super.heal(this);
                            super.say('gg', true);
                        }
                    } else {
                        super.say('gg', true);
                    }

                }

            }

        }

    }

}

module.exports = Reaper;