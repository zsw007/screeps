var Unit = require('./Unit');
var UnitTypes = require('./UnitTypes');
var Definitions = require('../definitions');
require('../plugins/RoomPositionPlugin');

/**
 * The Mobile Utility Lunar Excavator (MULE) is responsible for continuous saturation of an energy source by harvesting
 * from the energy source without changing position. Harvested energy is either deposited inside a StructureContainer,
 * StructureStorage or StructureLink if it exists within a range of 1 from the MULE. Otherwise it is dropped to the
 * ground.
 */
class MULE extends Unit {

    constructor(creep) {

        super(creep);

        this.memory.unit = UnitTypes.MULE;
        this.memory.job = this.memory.job || { source: null, storage: null };

    }

    /**
     * Moves to `source` and harvest from it. If the Source is invalid, will default to harvesting the source in
     * memory. If there is no source in memory, the nearest source will be harvested from. Returns OK while moving or
     * harvesting, or one of the ERR_* constants otherwise.
     *
     * @param  {(Source|String)=} source the Source to harvest from, or the id of the Source.
     * @return {Number} OK if moving or harvesting, or one of the ERR_* constants.
     */
    harvest(source) {

        // If we gave the id instead of the actual object, we'll just convert it.
        if (!(source instanceof Source) && _.isString(source)) {
            source = Game.getObjectById(source);
        }

        // If the source we gave was simply invalid, and we have a source in memory, we'll use that.
        if (!(source instanceof Source) && this.memory.job.source !== null) {
            source = Game.getObjectById(this.memory.job.source);
        }

        // If our source in memory is also invalid, we'll try to find one automatically.
        if (!(source instanceof Source)) {
            source = this.pos.findClosestByRange(FIND_SOURCES);
            this.memory.job.source = null;
        }

        var harvest = super.harvest(source);

        if (harvest === ERR_NOT_IN_RANGE) {
            return super.moveTo(source);
        }

        return harvest;

    }

    /**
     * Deposits everything carried into the `storage`. If the storage is invalid, will default to depositing to the
     * storage in memory. If there is no storage in memory, the nearest one will be deposited to. Returns OK if
     * successful, ERR_NOT_IN_RANGE if there is no nearby storage, or one of the ERR_* constants otherwise.
     *
     * @param  {Structure|Number} storage a storage structure that can be deposited into, or the id of the structure.
     * @return {Number} OK, or one of the ERR_* constants.
     */
    deposit(storage) {

        // If we gave the id instead of the actual object, we'll just convert it.
        if (!Definitions.isStorage(storage) && _.isString(storage)) {
            storage = Game.getObjectById(storage);
        }

        // If the storage we gave was simply invalid, and we have a storage in memory, we'll use that.
        if (!Definitions.isStorage(storage) && this.memory.job.storage !== null) {
            storage = Game.getObjectById(this.memory.job.storage);
        }

        // If our storage in memory is also invalid, we'll try to find one automatically.
        if (!Definitions.isStorage(storage)) {
            storage = this.pos.findClosestByRange(FIND_STRUCTURES, (structure) => Definitions.isStorage(structure));
            this.memory.job.storage = null;
        }

        var transfer = super.transfer(storage, RESOURCE_ENERGY);

        // If we are not in range of the storage yet we are harvesting from the source, we could be simply standing on
        // the wrong side.
        if (transfer === ERR_NOT_IN_RANGE) {

            var source = this.source;

            // Make sure the storage is actually within valid range of the source and that there is actually somewhere
            // to move to that is in range of both the source and the storage.
            if (storage.pos.inRangeTo(source, 2) && storage.pos.inStepTo(source, 1)) {
                return super.moveTo(storage);
            }

        }

        return transfer;
    }

    run() {

        if (super.goto(this.memory.goto) !== OK) {
            return OK;
        }

        if (this.harvest(this.source) === OK) {
            if (this.memory.job.storage === null || this.deposit(this.source) !== OK) {
                super.drop(RESOURCE_ENERGY);
            }
        }

        return OK;

    }

    /**
     * Assign a `source` to harvest from.
     *
     * @param  {Source} source the source to harvest from.
     * @return {Number} OK or one of the ERR_* constants.
     */
    assignSource(source) {

        if (!(source instanceof Source)) {
            return ERR_INVALID_TARGET;
        }

        this.memory.job.source = source.id;

        return OK;
    }

    /**
     * Assign a `storage` to deposit to.
     *
     * @param  {Source} storage the storage to deposit to.
     * @return {Number} OK or one of the ERR_* constants.
     */
    assignStorage(storage) {

        if (!(storage instanceof Structure) || !Definitions.isStorage(storage)) {
            return ERR_INVALID_TARGET;
        }

        if (this.memory.job.source) {

            var source = this.source;

            // If the storage is not within range 2 of the source, then it would be impossible to deposit into the
            // storage without changing position.
            if (source && !source.pos.inRangeTo(storage, 2)) {
                return ERR_NOT_IN_RANGE;
            }

        }

        this.memory.job.storage = storage.id;

        return OK;
    }

    static isMULE(creep) {

        if (creep instanceof Creep === false) {
            return false;
        }

        return creep instanceof MULE || creep.memory.unit === UnitTypes.MULE;
    }

    /**
     * Make the body, name, and memory of a MULE that can be spawned.
     *
     * @param  {[Number]}  body    the body array.
     * @param  {Source}    source  the source the MULE is to harvest from.
     * @param  {Structure} storage the storage the MULE is to deposit to.
     * @return {Object} the body, name, and memory of the MULE that should be spawned.
     */
    static make(body, source, storage) {
        // Storage doesn't have to be defined. This is a easy way to set it to null.
        if (!storage) {
            storage = {
                id: null
            };
        }

        return {
            body: body,
            name: undefined,
            memory: {
                unit: UnitTypes.MULE,
                goto: source.pos,
                job: {
                    source: source.id,
                    storage: storage.id,
                }
            }
        };
    }

    get source() {
        if (this.memory.job.source === null) {
            return null;
        }

        return Game.getObjectById(this.memory.job.source);
    }

    set source(source) {
        if (source instanceof Source) {
            this.memory.job.source = source.id;
        } else if (_.isString(source)) {
            this.memory.job.source = source;
        } else {
            this.memory.job.source = null;
        }
    }

    get storage() {
        if (this.memory.job.storage === null) {
            return null;
        }

        return Game.getObjectById(this.memory.job.storage);
    }

    set storage(storage) {
        if (storage instanceof Structure) {
            this.memory.job.storage = storage.id;
        } else if (_.isString(storage)) {
            this.memory.job.storage = storage;
        } else {
            this.memory.job.storage = null;
        }
    }

}

module.exports = MULE;