module.exports = {
    SCV: 'scv',
    MULE: 'mule',
    ZERGLING: 'zergling',
    REAPER: 'reaper',
    UNKNOWN: 'unknown',
};