var Unit = require('./Unit');
var UnitTypes = require('./UnitTypes');
var Definitions = require('../definitions');
var __ = require('../libs/lodash');

class SCV extends Unit {

    constructor(creep) {
        super(creep);

        // If we assign this creep as a worker, then better make sure it is a worker.
        this.memory.unit = UnitTypes.SCV;

        this.memory.job = this.memory.job || {
                role: null,
                target: null,
                now: null,
                energy: {
                    source: {
                        id: null, // id of the source to harvest from if set.
                        harvesting: false, // Whether currently harvesting or not.
                    },
                    storage: null, // The storage to withdraw energy from if set.
                    resource: null, // The location of dropped energy if set.
                }
        };
    }

    /**
     * Harvest from the `source` until carry capacity is reached. Returns OK if still harvesting, or ERR_FULL if carry
     * capacity is reached, or other ERR_* constants.
     *
     * @param  {Source} source the source to harvest from.
     * @return {Number} OK if still harvesting, or ERR_FULL if carry capacity is reached, or other ERR_* constants.
     */
    harvest(source) {
        if (this.carry.energy === this.carryCapacity) {
            return ERR_FULL;
        }

        // If we gave the id instead of the actual object, we'll just convert it.
        if (!(source instanceof Source) && _.isString(structure)) {
            source = Game.getObjectById(source);
        }

        // If the object is simply invalid, we'll use what we have in memory if it is valid.
        if (!(source instanceof Source) && this.memory.job.energy.source.id) {
            source = Game.getObjectById(this.memory.job.energy.source.id);
        }

        // If we have nothing valid in memory either, we'll try to find one automatically.
        if (!(source instanceof Source)) {
            source = this.pos.findClosestByRange(FIND_SOURCES, {
                filter: (source) => source.energy > 0
            });
            this.memory.job.energy.source.id = null;
        }

        if (this.carry.energy < this.carryCapacity && this.memory.job.energy.source.harvesting) {
            var harvest = super.harvest(source);

            if (harvest === ERR_NOT_IN_RANGE) {
                return super.moveTo(source);
            }

            return harvest;
        } else if (this.carry.energy === 0 && !this.memory.job.energy.source.harvesting) {
            this.memory.job.energy.source.harvesting = true;
            return OK;
        } else {
            this.memory.job.energy.source.harvesting = false;
            return ERR_FULL;
        }
    }

    /**
     * Deposit everything carried into the `structure`. Returns OK if everything goes well, or one of the ERR_*
     * constant. Upon completion, switches back into harvesting mode.
     *
     * @param {Structure} structure a structure that can be deposited into.
     * @return OK or one of the ERR_* constants.
     */
    deposit(structure) {

        if (structure === null || structure instanceof Structure === false) {
            return ERR_INVALID_TARGET;
        }

        if (!structure.energyCapacity) {
            return ERR_INVALID_TARGET;
        }

        if (structure.energy === structure.energyCapacity) {
            this.memory.job.target = null;
            this.memory.job.now = null;
            return ERR_FULL;
        }

        super.say('depositing');

        if (this.carry.energy > 0) {

            var tryDeposit;
            if((tryDeposit = super.transfer(structure, RESOURCE_ENERGY)) === ERR_NOT_IN_RANGE) {
                super.moveTo(structure);
                return OK;
            }

            return tryDeposit;

        } else {
            this.memory.job.role = null;
            this.memory.job.target = null;
            this.memory.job.now = null;
            return ERR_NOT_ENOUGH_RESOURCES;
        }



    }

    /**
     * Builds the construction `site`. Returns OK if everything goes well, or one of the ERR_*
     * constant. Upon completion, switches back into harvesting mode.
     *
     * @param {ConstrcutionSite} site a construction site that can be built.
     * @return OK or one of the ERR_* constants.
     */
    build(site) {

        if (site === null || site instanceof ConstructionSite === false) {
            return ERR_INVALID_TARGET;
        }

        super.say('building');

        if (this.carry.energy > 0) {

            var tryBuild;
            if ((tryBuild = super.build(site)) === ERR_NOT_IN_RANGE || tryBuild === OK) {
                super.moveTo(site);
                return OK;
            }

            return tryBuild;
        } else {
            this.memory.job.role = null;
            this.memory.job.target = null;
            this.memory.job.now = null;
            return ERR_NOT_ENOUGH_RESOURCES;
        }

    }

    /**
     * Upgrades the `controller`. Returns OK if everything goes well, or one of the ERR_*
     * constant. Upon completion, switches back into harvesting mode.
     *
     * @param {StructureController} controller a controller.
     * @return OK or one of the ERR_* constants.
     */
    upgrade(controller) {

        if (controller === null || controller instanceof StructureController === false) {
            return ERR_INVALID_TARGET;
        }

        super.say('upgrading');

        if (this.carry.energy > 0) {

            var tryUpgrade;
            if ((tryUpgrade = super.upgradeController(controller)) === ERR_NOT_IN_RANGE  || tryUpgrade === OK) {
                super.moveTo(controller);
                return OK;
            }

            return tryUpgrade;
        } else {
            this.memory.job.role = null;
            this.memory.job.target = null;
            this.memory.job.now = null;
            return ERR_NOT_ENOUGH_RESOURCES;
        }

    }

    /**
     * Repairs the `structure`. Returns OK if everything goes well, or one of the ERR_*
     * constant. Upon completion, switches back into harvesting mode.
     *
     * @param {Structure} structure a structure that can be repaired.
     * @return OK or one of the ERR_* constants.
     */
    repair(structure) {

        if (structure === null || structure instanceof Structure === false) {
            return ERR_INVALID_TARGET;
        }

        if (structure.hits === structure.hitsMax) {
            return ERR_FULL;
        }

        super.say('repairing');

        if (this.carry.energy > 0) {

            var tryRepair;
            if((tryRepair = super.repair(structure)) === ERR_NOT_IN_RANGE) {
                super.moveTo(structure);
                return OK;
            }

            return tryRepair;

        } else {
            this.memory.job.role = null;
            this.memory.job.target = null;
            this.memory.job.now = null;
            return ERR_NOT_ENOUGH_RESOURCES;
        }

    }

    /**
     * Picks up as much energy as the SCV can carry from the ground.
     *
     * @param {Resource} resource a dropped energy
     * @return {Number} OK while picking up or moving, or one of the ERR_* constants otherwise.
     */
    pickup(resource) {
        if (this.carry.energy > 0) {
            return ERR_FULL;
        }

        // If we gave the id instead of the actual object, we'll just convert it.
        if (!(resource instanceof Resource) && _.isString(resource)) {
            resource = Game.getObjectById(resource);
        }

        // If the object is simply invalid, we'll use what we have in memory if it is valid.
        if (!(resource instanceof Resource) && this.memory.job.energy.resource) {
            resource = Game.getObjectById(this.memory.job.energy.resource);
        }

        // If we have nothing valid in memory either, we'll try to find one automatically.
        if (!(resource instanceof Resource)) {
            resource = this.pos.findClosestByRange(FIND_DROPPED_RESOURCES, {
                filter: (resource) => resource.resourceType === RESOURCE_ENERGY
            });
            this.memory.job.energy.resource = null;
        }

        var pickup = super.pickup(resource);

        if (pickup === ERR_NOT_IN_RANGE) {
            super.moveTo(resource);
        }

        if (pickup === OK) {
            return ERR_FULL;
        }

        return pickup;
    }

    /**
     * Withdraws as much energy as the SCV can carry from the structure.
     *
     * @param {(Structure|String)=} structure a structure where energy can be withdrawn from.
     * @return {Number} OK while withdrawing or moving, or one of the ERR_* constants otherwise.
     */
    withdraw(structure) {
        if (this.carry.energy > 0) {
            return ERR_FULL;
        }

        // If we gave the id instead of the actual object, we'll just convert it.
        if (!(structure instanceof Structure) && _.isString(structure)) {
            structure = Game.getObjectById(structure);
        }

        // If the object is simply invalid, we'll use what we have in memory if it is valid.
        if (!(structure instanceof Structure) && this.memory.job.energy.storage) {
            structure = Game.getObjectById(this.memory.job.energy.storage);
        }

        // If we have nothing valid in memory either, we'll try to find one automatically.
        if (!(structure instanceof Structure)) {
            structure = this.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => definitions.isStorage(structure) && definitions.hasEnergy(hasEnergy)
            });
            this.memory.job.energy.storage = null;
        }

        var withdraw = super.withdraw(structure, RESOURCE_ENERGY, this.carryCapacity - this.carry);

        if (withdraw === ERR_NOT_IN_RANGE) {
            return super.moveTo(structure);
        }

        if (withdraw === OK) {
            return ERR_FULL;
        }

        return withdraw;
    }

    checkSelf() {


        // If we can't carry, then there isn't anything we can do, so try to conserve space and CPU.
        if (this.getActiveBodyparts(CARRY) === 0) {
            this.suicide();
            return ERR_BUSY;
        }

        // If we can't work, but we can still move and can still carry, let's just deposit.
        if (this.getActiveBodyparts(WORK) === 0 && this.getActiveBodyparts(MOVE) > 0 && this.carry.energy > 0) {

            var nearestDepot = this.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (structure) => Definitions.isDepot(structure) && structure.energy < structure.energyCapacity
            });

            if (nearestDepot) {
                this.assignDepositer(nearestDepot);
            } else {
                this.suicide();
            }

            return ERR_BUSY;
        }

        // If we can't move, but can still work and carry, then lets use the remaining energy to repair.
        if (this.getActiveBodyparts(MOVE) === 0 && this.getActiveBodyparts(WORK) > 0 && this.carry.energy > 0) {
            var nearestStructure = this.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (structure) => structure.hits < structure.hitsMax && this.pos.inRangeTo(structure, 3)
            });

            if (nearestStructure) {
                this.assignRepairer(nearestStructure);
            } else {
                this.suicide();
            }

            return ERR_BUSY;
        }

        // If we can't move and can't work, try and deposit it in a depot within 1 range from us.
        if (this.getActiveBodyparts(MOVE) === 0 && this.getActiveBodyparts(WORK) === 0 && this.carry.energy > 0) {
            var nearestDepot = this.pos.findInRange(FIND_MY_STRUCTURES, 1, {
                filter: (structure) => Definitions.isDepot(structure) && structure.energy < structure.energyCapacity
            });

            if (nearestDepot) {
                this.assignDepositer(nearestDepot);
            } else {
                this.suicide();
            }

            return ERR_BUSY;
        }

        // If we can't work or can't move, but we aren't carrying anything, then might as well conserve space and CPU.
        if ((this.getActiveBodyparts(MOVE) === 0 || this.getActiveBodyparts(WORK) === 0) && this.carry.energy === 0) {
            this.suicide();
            return ERR_BUSY;
        }

        return OK;
    }

    run() {
        if (super.goto(this.memory.goto) !== OK) {
            return;
        }

        //if (this.checkSelf() !== OK) {
            // Critical failure, salvage what we can and override default job.
        //    this.doJob();
        //    return;
        //}

        var energy;
        if (this.memory.job.energy.storage) {
            energy = this.withdraw(Game.getObjectById(this.memory.job.energy.storage));
        } else if (this.memory.job.energy.resource) {
            energy = this.pickup(Game.getObjectById(this.memory.job.energy.resource));
        } else if (this.memory.job.energy.source.id) {
            energy = this.harvest(Game.getObjectById(this.memory.job.energy.source.id));
        }

        if (energy === ERR_FULL) {
            this.memory.job.energy.storage = null;
            this.memory.job.energy.resource = null;
            this.memory.job.energy.source.id = null;

            if (this.isUpgrader()) {
                var controller = Game.getObjectById(this.memory.job.target) || this.room.controller;
                return this.upgrade(controller);
            }

            if (this.isDepositer()) {
                var depot = Game.getObjectById(this.memory.job.target)
                    || Game.getObjectById(this.memory.job.now)
                    || this.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return Definitions.isDepot(structure) && structure.energy < structure.energyCapacity;
                        }
                    });

                if (depot) {
                    this.memory.job.now = depot.id;
                    return this.deposit(depot);
                }

                return ERR_INVALID_ARGS;
            }

            if (this.isBuilder()) {
                var site = Game.getObjectById(this.memory.job.target)
                    || Game.getObjectById(this.memory.job.now)
                    || this.pos.findClosestByRange(FIND_CONSTRUCTION_SITES);


                if (site) {
                    this.memory.job.now = site.id;
                    return this.build(site);
                }

                return ERR_INVALID_ARGS;
            }

            if (this.isRepairer()) {
                var structure = Game.getObjectById(this.memory.job.target)
                    || Game.getObjectById(this.memory.job.now)
                    || this.pos.findClosestByRange(FIND_MY_STRUCTURES, {
                        filter: (structure) => {
                            return structure.hits < structure.hitsMax;
                        }
                    });

                if (structure) {
                    this.memory.job.now = structure.id;
                    return this.repair(structure);
                }

                return ERR_INVALID_ARGS;
            }

            if (this.isMaintainer()) {
                // Maintainers are responsible for maintenance of structures that decay, and walls. This is performed
                // automatically without command from a higher executor.
                var structures = this.room.find(FIND_STRUCTURES, {
                    filter: (structure) => Definitions.maintenanceRequired(structure)
                    && structure.hits < structure.hitsMax
                });

                if (structures.length > 0) {
                    var structure = __.minBy(structures, (structure) => _.ceil(structure.hits / structure.hitsMax, 1));
                    return this.repair(structure);
                }

                return ERR_INVALID_ARGS;
            }

            return ERR_INVALID_ARGS;
        }
    }


    isUpgrader() {
        return this.memory.job.role === 'upgrade';
    }

    isBuilder() {
        return this.memory.job.role === 'build';
    }

    isDepositer() {
        return this.memory.job.role === 'deposit';
    }

    isMaintainer() {
        return this.memory.job.role === 'maintain';
    }

    isRepairer() {
        return this.memory.job.role === 'repair';
    }

    hasEnergySource() {
        return this.memory.job.energy.source.id || this.memory.job.energy.storage || this.memory.job.energy.resource;
    }

    isIdle() {
        return 'job' in this.memory && 'role' in this.memory.job && this.memory.job.role === null;
    }

    /**
     * Suggest a `source` for this `worker` to harvest from. The suggestion is only followed if the worker is not
     * already harvesting.
     *
     * @param {Source} source the suggested source to harvest from.
     */
    assignSource(source) {

        if (source === null || source instanceof Source === false) {
            return ERR_INVALID_TARGET;
        }

        this.memory.job.energy.source.id = source.id;
    }


    assignStorage(storage) {

        if (storage === null || storage instanceof Structure === false) {
            return ERR_INVALID_TARGET;
        }

        this.memory.job.energy.storage = storage.id;
    }


    assignResource(resource) {

        if (resource === null || resource instanceof Resource === false) {
            return ERR_INVALID_TARGET;
        }

        this.memory.job.energy.resource = resource.id;
    }

    /**
     * Make the `worker` an upgrader, and assign it to upgrade specifically the `target`, if specified.
     *
     * @param {StructureController=} target the target to upgrade.
     * @return OK or one of the ERR_* constants.
     */
    assignUpgrader(target) {

        if (typeof target !== 'undefined') {
            if (target === null || target instanceof StructureController === false ||
                target.structureType !== STRUCTURE_CONTROLLER) {
                return ERR_INVALID_TARGET;
            }

            if (!target.my) {
                return ERR_NOT_OWNER;
            }

            this.memory.job.target = target.id;
        }


        this.memory.job.role = 'upgrade';


        return OK;
    }

    /**
     * Make the `worker` a builder, and assign it to work on specifically the `target`, if specified.
     *
     * @param {ConstructionSite=} target the target to build.
     * @return OK or one of the ERR_* constants.
     */
    assignBuilder(target) {

        if (typeof target !== 'undefined') {
            if (target === null || target instanceof ConstructionSite === false) {
                return ERR_INVALID_TARGET;
            }

            if (!target.my) {
                return ERR_NOT_OWNER;
            }

            this.memory.job.target = target.id;
        }

        this.memory.job.role = 'build';


        return OK;
    }

    /**
     * Make the `worker` a depositer, and assign it to deposit specifically into the `target`, if specified.
     *
     * @param {Structure=}   target the target to deposit. Should be a Structure that can store something.
     * @return OK or one of the ERR_* constants.
     */
    assignDepositer(target) {

        if (typeof target !== 'undefined') {
            if (target === null || target instanceof Structure === false || !Definitions.isDepot(target)) {
                return ERR_INVALID_TARGET;
            }

            this.memory.job.target = target.id;
        }

        this.memory.job.role = 'deposit';

        return OK;
    }

    /**
     * Make the `worker` a maintainer. Maintainers are a very general and very automated role, so it doesn't make sense
     * to specify a target
     *
     * @return OK or one of the ERR_* constants.
     */
    assignMaintainer() {

        this.memory.job.role = 'maintain';

        return OK;
    }

    /**
     * Make the `worker` a repairer, and assign it to repair specifically into the `target`, if specified.
     *
     * @param {Structure=}   target the target to deposit. Should be a Structure that can be repaired.
     * @return OK or one of the ERR_* constants.
     */
    assignRepairer(target) {

        if (typeof target !== 'undefined') {
            if (target === null || target instanceof Structure === false) {
                return ERR_INVALID_TARGET;
            }

            this.memory.job.target = target.id;
        }

        this.memory.job.role = 'repair';

        return OK;
    }

    static isSCV(creep) {

        if (creep instanceof Creep === false) {
            return false;
        }

        return creep instanceof SCV || creep.memory.unit === UnitTypes.SCV;
    }

    /**
     * Make the body, name, and memory of a SCV that can be spawned.
     *
     * @param  {[Number]}  body    the body array.
     * @param  {RoomObject}      room    the room the SCV is meant to operate in.
     * @return {Object} the body, name, and memory of the SCV that should be spawned.
     */
    static make(body, room) {
        return {
            body: body,
            name: undefined,
            memory: {
                unit: UnitTypes.SCV,
                goto: room.pos,
                job: {
                    role: null,
                    target: null,
                    now: null,
                    energy: {
                        source: {
                            id: null, // id of the source to harvest from if set.
                            harvesting: false, // Whether currently harvesting or not.
                        },
                        storage: null, // The storage to withdraw energy from if set.
                        resource: null, // The location of dropped energy if set.
                    }
                }
            }
        };
    }


}

module.exports = SCV;