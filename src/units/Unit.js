var Definitions = require('../definitions');
require('../plugins/RoomPositionPlugin');

class Unit extends Creep {

    constructor(creep) {
        super(creep.id);

        // The unit designation.
        this.memory.unit = this.memory.unit || 'unknown';

        // The destination room for where the Unit is meant to perform its job.
        this.memory.goto = this.memory.goto || null;

        // The room executor responsible for requesting this Unit to be spawned.
        this.memory.from = this.memory.from || null;

        // The job of the unit and job specific data.
        this.memory.job = this.memory.job || {};
    }

    run() {
        console.log('Creep ' + this.name + ' has invalid Unit type ' + this.memory.unit);
        Game.notify('Creep ' + this.name + ' has invalid Unit type ' + this.memory.unit, 30);
    }

    static make(body) {
        return {
            body: body,
            name: undefined,
            memory: {
                unit: 'unknown',
                goto: null,
                from: null,
                job: {},
            },
        };
    }

    /**
     * Go to the designated `room`.
     *
     * `room` can be given as either a Room object, a JSON.stringify then JSON.parse Room object, a RoomPosition within
     * a Room, or a String indicating the name of the Room. The actual coordinates does not matter, as we only intend to
     * reach the Room. Returns OK if in the Room, ERR_BUSY if still traveling, ERR_INVALID_TARGET if `room` is not a
     * valid Room to go to, or another ERR_* constant if none of the above.
     *
     * @param  {Room|RoomPosition|String|Object} room the room to go to.
     * @return                                        OK if in the Room, ERR_BUSY if still traveling, or
     *                                                ERR_INVALID_TARGET if `room` is not one of the above valid
     *                                                argument type or another ERR_* constant.
     */
    goto(room) {

        if (room === undefined || room === null) {
            return ERR_INVALID_TARGET;
        }

        var pos;
        if (room instanceof Room) {
            pos = room.getPositionAt(25, 25);
        } else if (_.isString(room)) {
            pos = new RoomPosition(25, 25, room);
        } else if (room instanceof RoomPosition) {
            pos = room;
        } else if (_.isObject(room) && 'x' in room && 'y' in room && 'roomName' in room) {
            pos = new RoomPosition(room.x, room.y, room.roomName);
        } else {
            return ERR_INVALID_TARGET;
        }

        if (this.room.name !== pos.roomName) {

            var move = super.moveTo(pos);
            if (move === OK || move === ERR_TIRED) {
                return ERR_BUSY;
            }

            return move;
        }

        return OK;

    }

    /**
     * Find the optimal path to the target within the same room and move to it. Overrides the parent moveTo command to
     * make improvements but will call the parent moveTo at the end.
     *
     * @override
     */
    moveTo(a, b, c) {
        if (this.getActiveBodyparts(MOVE) === 0) {
            return ERR_NO_BODYPART;
        }

        // Determine whether the arguments are moveTo(x, y, opts) or moveTo(target, opts).
        var target = null;
        var opts = {};
        if (_.isNumber(a) && _.isNumber(b)) {
            target = this.room.getPositionAt(a, b);
            opts = c;
        } else if (a.pos) {
            target = a.pos;
            opts = b;
        } else if (a instanceof RoomPosition) {
            target = a;
            opts = b;
        } else {
            return ERR_INVALID_TARGET;
        }

        if (typeof opts === 'undefined') {
            opts = {};
        }

        // Calculate the terrain penalty based on the number of body parts.
        var roadCost = 0;
        var plainCost = 0;
        var swampCost = 0;

        var totalCarry = _.sum(this.carry);
        for (var part of this.body) {
            // Note that body parts with 0 hits left still contribute to fatigue.

            if (part.type === CARRY && totalCarry > 0) {
                // CARRY parts carry 50 each. If it isn't carrying at least 1, it doesn't contribute fatigue.
                roadCost += 1;
                plainCost += 2;
                swampCost += 10;

                // Use this to determine how many parts are carrying stuff.
                totalCarry -= 50;
            } else if (part.type === MOVE && part.hits > 0) {
                // Move parts decrease by a constant 2.
                roadCost -= 2;
                plainCost -= 2;
                swampCost -= 2;
            } else if (part.type !== CARRY) {
                roadCost += 1;
                plainCost += 2;
                swampCost += 10;
            }
        }

        // Costs less than 1 uses the default.
        if (roadCost < 1) {
            roadCost = 1;
        }

        if (plainCost < 1) {
            plainCost = 1;
        }

        if (swampCost < 1) {
            swampCost = 1;
        }

        // Normalize the cost so the least cost is 1.
        var minCost = Math.min(roadCost, plainCost, swampCost);
        roadCost = _.ceil(roadCost / minCost);
        plainCost = _.ceil(plainCost / minCost);
        swampCost = _.ceil(swampCost / minCost);

        // Costs greater than 254 are considered unwalkable.
        if (roadCost > 254) {
            roadCost = 254
        }

        if (plainCost > 254) {
            plainCost = 254
        }

        if (swampCost > 254) {
            swampCost = 254
        }

        opts.costCallback = (roomName, costMatrix) => {

            var room = Game.rooms[roomName];

            // If we have no vision to this room.
            if (!room) {
                return costMatrix;
            }

            var roomData = room.lookAtArea(0, 0, 49, 49, true);
            for (let pos of roomData) {
                let x = pos.x;
                let y = pos.y;
                let type = pos.type;
                let data = pos[type];

                if (Definitions.isObstacleAt(pos)) {
                    // If its definitely an obstacle, then nothing we can do but to say it is unwalkable.
                    costMatrix.set(x, y, 0xff);
                } else if (costMatrix.get(x, y) !== 0xff && costMatrix.get(x, y) !== roadCost
                    && type === LOOK_TERRAIN && data === Definitions.TERRAIN_SWAMP) {
                    // If it's a swamp, and there's no road or obstacles on it.
                    costMatrix.set(x, y, swampCost);
                } else if (costMatrix.get(x, y) !== 0xff && costMatrix.get(x, y) !== roadCost
                    && type === LOOK_TERRAIN && data === Definitions.TERRAIN_PLAIN) {
                    // If it's a plain, and there's no road or obstacles on it.
                    costMatrix.set(x, y, plainCost);
                } else if (costMatrix.get(x, y) !== 0xff &&
                    type === LOOK_STRUCTURES && data.structureType === STRUCTURE_ROAD) {
                    // If it's a road and there are no obstacles on it.
                    costMatrix.set(x, y, roadCost);
                }

            }

            return costMatrix;
        };



        return super.moveTo(target, opts);
    }

}

module.exports = Unit;