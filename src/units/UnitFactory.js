var Unit = require('./Unit');
var UnitTypes = require('./UnitTypes');
var SCV = require('./SCV');
var MULE = require('./MULE');
var Zergling = require('./Zergling');
var Reaper = require('./Reaper');

class UnitFactory {

    /**
     * Gets the Unit that associates with the given creep.
     * @param {Creep} creep the creep to get the Unit from.
     */
    static getUnit(creep) {

        switch (creep.memory.unit) {
            case UnitTypes.SCV:
                return new SCV(creep);
            case UnitTypes.MULE:
                return new MULE(creep);
            case UnitTypes.ZERGLING:
                return new Zergling(creep);
            case UnitTypes.REAPER:
                return new Reaper(creep);
        }

        return new Unit(creep);
    }

    static makeMULE(body, source, storage) {
        return MULE.make(body, source, storage);
    }

    static makeSCV(body, room) {
        return SCV.make(body, room);
    }

}

module.exports = UnitFactory;