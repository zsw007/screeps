var Unit = require('./Unit');
var UnitTypes = require('./UnitTypes');

class Zergling extends Unit {


    patrol() {
        var randomX = Math.floor(Math.random() * (49 - 0 + 1) + 0);
        var randomY = Math.floor(Math.random() * (49 - 0 + 1) + 0);
        var direction = this.pos.getDirectionTo(randomX, randomY);
        super.move(direction);
    }

    run() {
        var targets = this.room.find(FIND_HOSTILE_CREEPS);

        if(targets.length) {
            if(super.attack(targets[0]) === ERR_NOT_IN_RANGE) {
                super.moveTo(targets[0]);
            }

            super.say('attacking');
        } else {
            super.say('patrolling');
            this.patrol();
        }
    }

}

module.exports = Zergling;